const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const {Server} = require("socket.io");
const io = new Server(server);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/start.html');
});

app.get('/room', (req, res) => {
    res.sendFile(__dirname + '/room.html');
});

app.get('/styles.css', (req, res) => {
    res.sendFile(__dirname + '/styles.css');
});

app.get('/emoji/fgEmojiPicker.js', (req, res) => {
    res.sendFile(__dirname + '/emoji/fgEmojiPicker.js');
});

const rooms = new Map();


io.on('connection', (socket) => {
    const {roomId: tempRoomId, username, color, textColor, status} = socket.handshake.query;
    const roomId = tempRoomId.toLowerCase();
    const userId = socket.id;
    socket.data.userDetails = {
        roomId,
        userId,
        username,
        color,
        textColor,
        status
    };

    socket.join(roomId);
    console.log(`user ${userId} connected and joined room ${roomId}`);

    const socketsInRoom = [...io.sockets.adapter.rooms.get(roomId)]
        .map(socketId => io.sockets.sockets.get(socketId))
    const usersDetails = socketsInRoom.map(socket => socket.data.userDetails);
    io.to(roomId).emit("status-update", usersDetails);

    socket.on('change-status', ({status}) => {
        console.log(`user ${socket.data.userDetails.username} changed the status to ${status}`);
        socket.data.userDetails.status = status;
        const socketsInRoom = [...io.sockets.adapter.rooms.get(roomId)]
            .map(socketId => io.sockets.sockets.get(socketId))
        const usersDetails = socketsInRoom.map(socket => socket.data.userDetails);
        io.to(roomId).emit("status-update", usersDetails);
    });

    socket.on("disconnecting", () => {
        console.log(`user ${userId} disconnected`);
        const socketsInRoom = [...io.sockets.adapter.rooms.get(roomId)]
            .map(socketId => io.sockets.sockets.get(socketId))
            .filter(socket => socket.data.userDetails.userId !== userId);
        const usersDetails = socketsInRoom.map(socket => socket.data.userDetails);
        io.to(roomId).emit("status-update", usersDetails);
    })
});

server.listen(80, () => {
    console.log('listening on *:80');
});