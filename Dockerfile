FROM node:17-alpine

COPY . /ajutor/
WORKDIR /ajutor

RUN npm ci

EXPOSE 80/tcp
CMD npm start